﻿//  Copyright (C) CompatibL Technologies LLC. All rights reserved.
//  This code contains valuable trade secrets and may be used, copied,
//  stored, or distributed only in accordance with a written license
//  agreement and with the inclusion of this copyright notice. 

using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin.Cors;
using Owin;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;

namespace TestApp
{
    class Startup
    {
        public void Configuration( IAppBuilder appBuilder )
        {
            //Static files hosting 
            var physicalFileSystem = new PhysicalFileSystem( @"StaticFiles\" );
            var options = new FileServerOptions
            {
                EnableDefaultFiles = true,
                FileSystem = physicalFileSystem
            };
            options.StaticFileOptions.FileSystem = physicalFileSystem;
            options.StaticFileOptions.ServeUnknownFileTypes = true;
            options.DefaultFilesOptions.DefaultFileNames = new[] { "Index.html" };
            appBuilder.UseFileServer( options );

            //SignalR
            appBuilder.UseCors( CorsOptions.AllowAll );
            appBuilder.MapSignalR();
        }
    }

    [HubName("msg")]
    public class MyHub : Hub
    {
        public void Send(string name, string message)
        {
            Clients.All.addMessage(name, message);
        }
    }

}