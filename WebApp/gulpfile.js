'use strict';

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    watch = require('gulp-watch'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    cssmin = require('gulp-minify-css'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload,
	sourcemaps = require('gulp-sourcemaps');

var streamqueue  = require('streamqueue');

var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
        fonts: 'build/fonts/',
        libs: 'build/libs/',
    },
    src: {
        html: 'src/*.html',
        js: 'src/scripts/**/*.js',
        style: 'src/styles/**/.css',
        img: 'src/imgs/**/*.*',
        fonts: 'src/fonts/**/*.*',
        libs: 'bower_components/**/*.min.js'
    },
    watch: {
        html: 'src/*.html',
        js: 'src/scripts/**/*.js',
        style: 'src/styles/**/*.css',
        img: 'src/imgs/**/*.*',
        fonts: 'src/fonts/**/*.*',
        libs: 'bower_components/**/*.min.js'
    },
    clean: './build'
};

var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: false,
    host: 'localhost',
    port: 2233,
    logPrefix: "Frontend_Devil"
};

gulp.task('html:build', function () {
    gulp.src(path.src.html) 
		.pipe(sourcemaps.init())
		.pipe(concat('index.html'))
		.pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(path.build.html)) 
		.pipe(reload({stream: true})); 
});

gulp.task('js:build', function () {

    gulp.src(path.src.js)
		.pipe(sourcemaps.init())
		.pipe(concat('app.min.js'))//все файлы соберед в один с указанным именем
        .pipe(uglify())//офусцирует файл
		.pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(path.build.js))//переложит файл по указанному пути
		.pipe(reload({stream: true}));// перегрузит
});

gulp.task('libs:build', function() {
    return streamqueue({ objectMode: true },
        gulp.src('bower_components/jquery/dist/jquery.js'),
        gulp.src('bower_components/angular/angular.js'),
        gulp.src('bower_components/signalr/jquery.signalR.js'),
        gulp.src('path.src.libs')
    )
		.pipe(sourcemaps.init())
        .pipe(concat('libs.min.js'))
		.pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(path.build.libs))
        .pipe(reload({stream: true}));
});


gulp.task('style:build', function () {
    gulp.src(path.src.style) 
	    .pipe(concat('app.css'))
		.pipe(uglify())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});

gulp.task('build', [
    'html:build',
    'js:build',
    'style:build',
    'libs:build'
]);

gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.libs], function(event, cb) {
        gulp.start('libs:build');
    });
});

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', ['build', 'webserver', 'watch']);